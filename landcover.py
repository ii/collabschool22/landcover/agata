# -*- coding: utf-8 -*-
"""
Created on Tue Jun 21 13:19:39 2022

landcover, based on binary data describing the wordl map determine using 
a coordinates array if given coordinates fall on water or land

hdkarlsen@gmail.com

@author: Gimli
"""

import numpy as np
import math
import seaborn as sns

sns.set()
import matplotlib.pyplot as plt
import argparse


def command_line_parse():
    """parse command line input"""
    # Parser for command line options
    parser = argparse.ArgumentParser(description="Run the ABL single column model.")
    # Add arguments
    parser.add_argument("-fd", "--file_dir",
                        help="Specify path to data.",
                        action="store_true",
                        type=str,
                        default=False)

    return parser.parse_args()


# Read command line input
args = command_line_parse()
file_dir = args.file_dir

# file directory
if not file_dir:
    file_dir = r"C:\Users\Gimli\Documents\PhD\WorkshopsConferences\Files\gl-latlong-1km-landcover.bsq"

# reading the binary file with NumPy based on given file characteristics
numpy_map_data = np.fromfile(file_dir, dtype=np.int8)
print(numpy_map_data)

# reshaping the map to fit the expected pix frame
reshaped_map_data = np.reshape(numpy_map_data, (43200, 21600))

print(reshaped_map_data[:])

# plt.imshow(reshaped_map_data[::1000,::1000])

# Input of coordinates by user
x = float(input("Enter chosen latitude: "))
if x > 180 or x < -180:
    print("Value outside of the latitude range. Enter value between -180 and 180")
    x = float(input("Enter chosen latitude: "))

y = float(input("Enter chosen longitude: "))
if y > 180 or y < -180:
    print("Value outside of the longitude range. Enter value between -180 and 180")
    y = float(input("Enter chosen longitude: "))

print(x, y)

plt.title(
    "How to visualize (plot) \n a numpy array in python using seaborn ?", fontsize=12
)

plt.savefig("visualize_numpy_array_01.png", bbox_inches="tight", dpi=100)

plt.show()


# write a string within a first line of a function
def translate_coordinates(x, y):
    new_x = (x + 180) / 360 * 43199
    new_y = (y + 90) / 180 * 21599
    return math.floor(new_x), math.floor(new_y)


translate_coordinates(40, -50)


# find a cell
def land_finder(x, y):
    lat, long = translate_coordinates(x, y)
    place = reshaped_map_data[long, lat]
    if place == 0:
        print("water")
    else:
        print("land")





land_finder(0, 0)
